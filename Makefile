REPOSITORY ?= "vqfx"
VERSION ?= "15.1X53-D60"
IMAGE ?= $(REPOSITORY):$(VERSION)

CONSUL_VERSION ?= 0.7.2
CONSUL_TEMPLATE_VERSION ?= 0.16.0
#QCOW2_RE = "cirros-0.3.4-x86_64-disk_re.img"
QCOW2_RE = "vqfx-re-$(VERSION).qcow2"
QCOW2_PFE ?= "vqfx-pfe-$(VERSION).qcow2"
#QCOW2_PFE = "cirros-0.3.4-x86_64-disk_pfe.img"
RAM_RE = "2048"
RAM_PFE = "2048"
SOCKETS_RE = "1"
SOCKETS_PFE = "2"

BUILD_OPTIONS = -t $(IMAGE)
BUILD_OPTIONS += --build-arg CONSUL_VERSION=$(CONSUL_VERSION)
BUILD_OPTIONS += --build-arg CONSUL_TEMPLATE_VERSION=$(CONSUL_TEMPLATE_VERSION)
BUILD_OPTIONS += --build-arg QCOW2_RE=$(QCOW2_RE)
BUILD_OPTIONS += --build-arg QCOW2_PFE=$(QCOW2_PFE)

RUN_ENV = QCOW2_RE=$(QCOW2_RE)
RUN_ENV += QCOW2_PFE=$(QCOW2_PFE)
RUN_ENV += RAM_RE=$(RAM_RE)
RUN_ENV += RAM_PFE=$(RAM_PFE)
RUN_ENV += SOCKETS_RE=$(SOCKETS_RE)
RUN_ENV += SOCKETS_PFE=$(SOCKETS_PFE)
RUN_ENV += IMAGE=$(IMAGE)

default: up

system-check:
	echo "Checking docker and docker-compose version"
	docker -v
	docker-compose -v
	echo "Check that /dev/kvm exists"
	ls -al /dev/kvm
	echo "Check netsted virtualization capabilities"
	cat /sys/module/kvm_intel/parameters/nested
	modinfo kvm_intel | grep nested
	

convert-img:
	qemu-img convert -f vmdk -O qcow2 -c vqfx-pfe-15.1X53-D60.vmdk vqfx-pfe-15.1X53-D60.qcow2

build:
	docker build $(BUILD_OPTIONS) .

up:
	lsmod | grep l2tp_eth || echo "please load l2tp_eth first" && exit 1
	$(RUN_ENV) docker-compose up

%-spine:
	$(eval SPINES := $(patsubst %-spine,%,$@))
	$(RUN_ENV) docker-compose scale spine=$(SPINES)

%-leaf:
	$(eval LEAVES := $(patsubst %-leaf,%,$@))
	$(RUN_ENV) docker-compose scale leaf=$(LEAVES)

clean:
	$(RUN_ENV) docker-compose rm -f
	docker rmi ${IMAGE}
