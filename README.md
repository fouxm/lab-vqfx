# Problem I want to solve
I wanted to be able to run a vQFX on my laptop without installing Vagrant

I ended discovering the hard way that nested virtualization was not possible with Docker for Mac

So now I build a docker container with the vQFX RE and PFE included. This image can be started with docker-compose (wrapped in make targets)

Connectivity between interfaces are provided by l2tp tunnels. The ambition is to be able to deploy a spine-leaf topology spread through multiple nodes running docker swarm (leveraging Swarm overlay)


# pre-flight
To run this lab, you will need
* a VM or a bre-metal server running Linux (tried with Docker for Mac but KVM won't work)
* a docker engine running on the server
* minimal set of tools (git, make, docker-compose, ...)
* the vQFX vmdk or qcow2 images downloaded for Juniper website or obtained through your contact at Juniper
* check the netsted virtualization capabilities http://docs.openstack.org/developer/devstack/guides/devstack-with-nested-kvm.html
* load l2tp kernel module on the system hosting docker-engine


# Usage
Place the vqfx RE and PFE images in a folder called $(pwd-)/resources/images

Edit the Makefile to define the
* vQFX images name for both Routing Engine (RE) / Packet Forwarding Engine (PFE) (vars `QCOW2_RE` and `QCOW2_PFE`)
* memory / CPU you want to allocate to RE / PFE (vars `RAM_RE`, `RAM_PFE`, `SOCKET_RE`, `SOCKET_PFE`)
* vQFX version you wan to run (var `VERSION`)

[WIP] You need to convert the vmdk image into qcow2 format using

[WIP]  ```# make image-convert```

Then you can build Docker image into your local registry with
  ```# make build```

Stack is defined to start with one spine and one leaf. Spines and leaves are differentiated so you can unse `docker-compose scale` to scale each one individually
  ```# make up``` 

This operation takes time as it starts the vQFX VM(s) ~3-5 minutes depending on the number of VMs
The docker-compose output is a stream of every stdout process so we cannot enter interactive mode with it.


[WIP] Find a way to bind stdout to some kind of device that allows to get interactive with the QEMU process. If I remember well from another try, PFE shell gets to another window

# Accesses and config
Consul UI is reachable to http://localhost:8500 on the docker host
You also can query Consul API to list Services or nodes
An example to list all spines
   ```# curl http://localhost:8500/v1/catalog/service/vqfx-spine``` 

You can `ssh` the RE from the docker host using the IP obtained above
User is 'root', password is 'Juniper'
Once your are connected, type `cli` to get the system prompt

# Sources
  * http://www.linux-kvm.org/page/Networking
  * https://github.com/Ulexus/docker-qemu
  * http://kchard.github.io/runit-quickstart
  * https://wiki.archlinux.org/index.php/Network_bridge
  * http://www.deepspace6.net/docs/iproute2tunnel-en.html
  * http://baturin.org/docs/iproute2/ <- The BEST iproute2 documentation ever !!!
  * https://remote-lab.net/linux-l2tp-ethernet-pseudowires
  * https://github.com/Juniper/py-junos-netconify

# Known issues
  * LLDP does not work between nodes. LLDP uses dest mac 01:80:c2:00:00:0e that is not transported through the L2TP pseudowire
