#!/bin/bash
set -xe

# Install dependencies

apt-get update
apt-get upgrade -y
apt-get install -y \
  bridge-utils \
  curl \
  iproute2 \
  jq \
  iputils-ping \
  kmod \
  qemu-kvm \
  runit \
  ssh-client \
  tshark \
  unzip \
  vim

# Moving runit and consul configuration to /etc
mv /var/tmp/resources/etc/consul \
   /etc
mv /var/tmp/resources/etc/service/consul-agent \
   /etc/service
mv /var/tmp/resources/etc/service/consul-template \
   /etc/service
mv /var/tmp/resources/etc/service/vqfx-re \
   /etc/service
mv /var/tmp/resources/etc/service/vqfx-pfe \
   /etc/service
mv /var/tmp/resources/etc/runit \
   /etc
mv /var/tmp/resources/etc/consul-template.conf \
   /etc

# Move image to libvirt image folder
mkdir -p /var/lib/qcow2
mv /var/tmp/resources/images/${QCOW2_RE} /var/lib/qcow2/vqfx-re.qcow2

if [ -n "$QCOW2_PFE" ]
then
  mv /var/tmp/resources/images/${QCOW2_PFE} /var/lib/qcow2/vqfx-pfe.qcow2
fi

# Install ifup qemu script
#mv /var/tmp/resources/vqfx-kvm-start.sh /etc

# Create bridge.conf in /etc/qemu
mkdir -p /etc/qemu
echo "allow vqfx-bp" >> /etc/qemu/bridge.conf

for i in $(seq 0 11)
do
  echo "allow vqfx-dp-${i}" >> /etc/qemu/bridge.conf
done

# Install Consul
curl -s -o consul.zip https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
unzip consul.zip -d /usr/sbin
rm consul.zip
mkdir /var/lib/consul

# Install Consul Template
curl -s -o consul-template.zip https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip
unzip consul-template.zip -d /usr/sbin
rm consul-template.zip

# Install templates
mkdir /var/lib/consul-template
mv /var/tmp/resources/consul-template/* /var/lib/consul-template
