#!/bin/bash
set -x

RAM_PFE=${RAM_PFE:-1024}
SOCKETS_PFE=${SOCKETS_PFE:-4}

function new_mac() {
  echo "52:54:00:$(dd if=/dev/urandom bs=512 count=1 2>/dev/null | md5sum | sed 's/^\(..\)\(..\)\(..\).*$/\1:\2:\3/')"
}

exec /usr/bin/kvm \
  -enable-kvm \
  -nographic \
  -serial mon:stdio \
  -name $(hostname)_pfe \
  -machine pc-i440fx-trusty,accel=kvm,usb=off \
  -m ${RAM_PFE} \
  -realtime mlock=off \
  -smp ${SOCKETS_PFE},sockets=${SOCKETS_PFE},cores=1,threads=1 \
  -smbios type=0,vendor=Wistar \
  -smbios type=1,manufacturer=Wistar,product=Wistar-vqfx1-VM,version=2.0 \
  -no-user-config \
  -nodefaults \
  -rtc clock=host \
  -boot strict=on \
  -device piix3-usb-uhci,id=usb,bus=pci.0,addr=0x1.0x2 \
  -drive file=/var/lib/qcow2/vqfx-pfe.qcow2,format=qcow2,if=none,id=drive-ide0-0-0,cache=none,aio=native \
  -device ide-hd,bus=ide.0,unit=0,drive=drive-ide0-0-0,id=ide0-0-0,bootindex=1 \
  -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6 \
  -msg timestamp=on \
  -netdev user,id=bp-0,net=10.0.0.0/27,host=10.0.0.1,hostfwd=tcp::222-:22 \
  -device e1000,netdev=bp-0,mac=$(new_mac),bus=pci.0,addr=0x3 \
  -netdev bridge,id=bp-1,br=vqfx-bp \
  -device e1000,netdev=bp-1,mac=$(new_mac),bus=pci.0,addr=0x4

