FROM ubuntu:16.04
MAINTAINER Foucault de Bonneval <foucault(at)commit.ninja>
ENV DEBIAN_FRONTEND noninteractive

ARG QCOW2_RE
ARG QCOW2_PFE
ARG CONSUL_VERSION
ARG CONSUL_TEMPLATE_VERSION

ADD resources /var/tmp/resources
RUN /var/tmp/resources/build.sh

ENV SVDIR="/etc/service"
EXPOSE 22 222

VOLUME /var/lib/qcow2

CMD ["/sbin/runit"]
